# Salvaorejas
Dispositivo para evitar heridas en la parte posterior de la oreja al usar las mascarillas quirurgicas durante largos periodos de tiempo


## SalvaOrejas Mod
**Proceso:** Impresión 3D

**Material:** PLA

![](./Imgs/SalvaOrejasMod1.png)

## SalvaOrejas Modelo Sant Cugat 
**Proceso:** Cortadora laser

**Material:** Plakene

<img src="./Imgs/SalvaOreja_s16_FLSC.png " width="500">
